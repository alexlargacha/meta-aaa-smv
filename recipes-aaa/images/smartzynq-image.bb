SUMMARY = "SmartZynq image"
DESCRIPTION = "Contents all need packages to deploy a rootfs for the SmartZynq"

LICENSE = "MIT"

inherit smartzynq-image
