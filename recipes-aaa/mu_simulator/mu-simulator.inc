SUMMARY = "MU Simulator"
HOMEPAGE = "https://gitlab.com/alexlargacha/mu_simulator"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS = "libpcap"
SRC_URI = "git://git@gitlab.com/alexlargacha/mu_simulator.git;protocol=ssh;branch=${MUSIMULATOR_BRANCH}"

S = "${WORKDIR}/git"

#PACKAGES = "${PN}"

ALLOW_EMPTY_${PN} = "1"

RDEPENDS_${PN} = "libpcap"

inherit cmake
