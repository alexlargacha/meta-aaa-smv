MUSIMULATOR_BRANCH ?= "master"

SRCREV = "${AUTOREV}"
PV = "${MUSIMULATOR_BRANCH}+git${SRCPV}"

EXTRA_OECMAKE += "-DWITH_GTK=OFF"

include mu-simulator.inc
