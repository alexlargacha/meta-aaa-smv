SMV_BRANCH ?= "master"

SRCREV = "${AUTOREV}"
PV = "${SMV_BRANCH}+git${SRCPV}"

include smv.inc
