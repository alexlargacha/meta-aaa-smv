SUMMARY = "SMV App"
HOMEPAGE = "https://gitlab.com/alexlargacha/smv_smartzynq"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "git://git@gitlab.com/alexlargacha/smv_smartzynq.git;protocol=ssh;branch=${SMV_BRANCH}"

S = "${WORKDIR}/git"

#PACKAGES = "${PN}"

ALLOW_EMPTY_${PN} = "1"

EXTRA_OECMAKE=""

inherit cmake

#do_install_append() {
#        install -m 0644 hellocmake ${D}/${bindir}
#}
