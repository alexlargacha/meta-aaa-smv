FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
 
PCW_DTSI            ?= "pcw.dtsi"
PL_DTSI             ?= "pl.dtsi"
SYSTEM_TOP_DTS      ?= "system-top.dts"
SYSTEM_CONF_DTSI    ?= "system-conf.dtsi"
  
SRC_URI += "file://${PCW_DTSI}"
SRC_URI += "file://${PL_DTSI}"
SRC_URI += "file://${SYSTEM_TOP_DTS}"
SRC_URI += "file://${SYSTEM_CONF_DTSI}"
   
do_configure_append() {
    cp ${WORKDIR}/${PCW_DTSI} ${B}/device-tree
    cp ${WORKDIR}/${PL_DTSI} ${B}/device-tree
    cp ${WORKDIR}/${SYSTEM_TOP_DTS} ${B}/device-tree
    cp ${WORKDIR}/${SYSTEM_CONF_DTSI} ${B}/device-tree
}
