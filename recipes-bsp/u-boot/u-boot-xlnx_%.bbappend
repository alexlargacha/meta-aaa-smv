FILESEXTRAPATHS_prepend := "${THISDIR}/u-boot-smartzynq:"
SRC_URI += "file://0001-arm-add-support-for-smartzynq-board.patch"

#unset DTB_NAME to avoid using external DTB in u-boot build
#meta-xilinx-tools/recipes-bsp/u-boot/u-boot-xlnx_%.bbappend
DTB_NAME = ""

HAS_PLATFORM_INIT_append = " \
    zynq_smart_defconfig \
    "

