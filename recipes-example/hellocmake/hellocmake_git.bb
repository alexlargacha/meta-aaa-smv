HELLOCMAKE_BRANCH ?= "master"

SRCREV = "${AUTOREV}"
PV = "${HELLOCMAKE_BRANCH}+git${SRCPV}"

include hellocmake.inc
