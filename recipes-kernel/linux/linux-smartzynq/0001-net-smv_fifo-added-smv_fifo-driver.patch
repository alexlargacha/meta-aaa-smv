From ae05375054af91e3689f45afc374abdee946b36e Mon Sep 17 00:00:00 2001
From: Alex Largacha <alexlargacha@gmail.com>
Date: Thu, 21 Nov 2019 18:52:07 +0100
Subject: [PATCH] net: smv_fifo: added smv_fifo driver

Signed-off-by: Alex Largacha <alexlargacha@gmail.com>
---
 drivers/net/Kconfig    |   3 +
 drivers/net/Makefile   |   1 +
 drivers/net/smv_fifo.c | 724 +++++++++++++++++++++++++++++++++++++++++
 drivers/net/smv_fifo.h |  30 ++
 4 files changed, 758 insertions(+)
 create mode 100644 drivers/net/smv_fifo.c
 create mode 100644 drivers/net/smv_fifo.h

diff --git a/drivers/net/Kconfig b/drivers/net/Kconfig
index d03775100f7d..b99c7c630912 100644
--- a/drivers/net/Kconfig
+++ b/drivers/net/Kconfig
@@ -70,6 +70,9 @@ config DUMMY
 	  To compile this driver as a module, choose M here: the module
 	  will be called dummy.
 
+config SMVDATA
+        tristate "SMVDATA driver support"
+
 config EQUALIZER
 	tristate "EQL (serial line load balancing) support"
 	---help---
diff --git a/drivers/net/Makefile b/drivers/net/Makefile
index 21cde7e78621..33421a801136 100644
--- a/drivers/net/Makefile
+++ b/drivers/net/Makefile
@@ -32,6 +32,7 @@ obj-$(CONFIG_GTP) += gtp.o
 obj-$(CONFIG_NLMON) += nlmon.o
 obj-$(CONFIG_NET_VRF) += vrf.o
 obj-$(CONFIG_VSOCKMON) += vsockmon.o
+obj-$(CONFIG_SMVDATA) += smv_fifo.o
 
 #
 # Networking Drivers
diff --git a/drivers/net/smv_fifo.c b/drivers/net/smv_fifo.c
new file mode 100644
index 000000000000..b69d115cf611
--- /dev/null
+++ b/drivers/net/smv_fifo.c
@@ -0,0 +1,724 @@
+
+/*
+ * smv_fifo.c --  SMV fifo driver
+ *
+ */
+
+#include <linux/module.h>
+#include <linux/init.h>
+#include <linux/moduleparam.h>
+
+#include <linux/sched.h>
+#include <linux/kernel.h> /* printk() */
+#include <linux/slab.h> /* kmalloc() */
+#include <linux/errno.h>  /* error codes */
+#include <linux/types.h>  /* size_t */
+#include <linux/interrupt.h> /* mark_bh */
+
+#include <linux/in.h>
+#include <linux/netdevice.h>   /* struct device, and other headers */
+#include <linux/etherdevice.h> /* eth_type_trans */
+#include <linux/ip.h>          /* struct iphdr */
+#include <linux/tcp.h>         /* struct tcphdr */
+#include <linux/skbuff.h>
+
+#include "smv_fifo.h"
+
+#include <linux/in6.h>
+#include <asm/checksum.h>
+
+
+#include <linux/of_platform.h>
+#define ALIGNMENT		4
+
+/* BUFFER_ALIGN(adr) calculates the number of bytes to the next alignment. */
+#define BUFFER_ALIGN(adr) ((ALIGNMENT - ((ulong)adr)) % ALIGNMENT)
+
+#define DRIVER_NAME "smv_fifo"
+
+#define VACANCYOFFSET 0xC
+
+#define MAX_DEVICES 4 // maximum number of interfaces
+
+int numberSMVFrames = 30;
+
+/*
+ * The devices
+ */
+
+struct net_device *smv_devs[MAX_DEVICES];
+static unsigned int counter = 0;
+/**
+ * struct fifo_local - Our private per device data
+ * @ndev:		instance of the network device
+ * @base_addr:		base address of the Emaclite device
+ 
+ */
+struct fifo_local {
+	struct net_device *ndev;
+	void __iomem *base_addr;
+	spinlock_t reset_lock; /* lock used for synchronization */
+};
+
+
+/*
+ * Transmitter lockup simulation, normally disabled.
+ */
+static int lockup = 0;
+module_param(lockup, int, 0);
+
+static int timeout = SMV_TIMEOUT;
+module_param(timeout, int, 0);
+
+/*
+ * Do we run in NAPI mode?
+ */
+static int use_napi = 0;
+module_param(use_napi, int, 0);
+
+
+/*
+ * A structure representing an in-flight packet.
+ */
+struct smv_packet {
+	struct smv_packet *next;
+	struct net_device *dev;
+	int	datalen;
+	u32 data[ETH_DATA_LEN];
+};
+
+int pool_size = 8;
+module_param(pool_size, int, 0);
+
+/*
+ * This structure is private to each device. It is used to pass
+ * packets in and out, so there is place for a packet
+ */
+
+struct smv_priv {
+	struct net_device_stats stats;
+	int status;
+	struct smv_packet *ppool;
+	struct smv_packet *rx_queue;  /* List of incoming packets */
+	int rx_int_enabled;
+	int tx_packetlen;
+	u32 *tx_packetdata;
+	struct sk_buff *skb;
+	spinlock_t lock;
+	struct net_device *dev;
+	struct napi_struct napi;
+};
+
+
+//static void (*smv_interrupt)(int, void *, struct pt_regs *);
+
+/*
+ * Set up a device's packet pool.
+ */
+void smv_setup_pool(struct net_device *dev)
+{
+	struct smv_priv *priv = netdev_priv(dev);
+	int i;
+	struct smv_packet *pkt;
+
+	priv->ppool = NULL;
+	for (i = 0; i < pool_size; i++) {
+		pkt = kmalloc (sizeof (struct smv_packet), GFP_KERNEL);
+		if (pkt == NULL) {
+			printk (KERN_NOTICE "Ran out of memory allocating packet pool\n");
+			return;
+		}
+		pkt->dev = dev;
+		pkt->next = priv->ppool;
+		priv->ppool = pkt;
+	}
+}
+
+
+static ssize_t numberSMVFrames_show(struct kobject *kobj, struct kobj_attribute *attr,
+                      char *buf)
+{
+        return sprintf(buf, "%d\n", numberSMVFrames);
+}
+static ssize_t numberSMVFrames_store(struct kobject *kobj, struct kobj_attribute *attr,
+                      const char *buf, size_t count)
+{
+        sscanf(buf, "%d", &numberSMVFrames);
+        return count;
+}
+
+static struct kobj_attribute numberSMVFrames_attribute =__ATTR(value, 0660, numberSMVFrames_show,
+                                                   numberSMVFrames_store);
+static struct kobject *numberSMVFrames_kobject;  
+
+
+
+void smv_teardown_pool(struct net_device *dev)
+{
+	struct smv_priv *priv = netdev_priv(dev);
+	struct smv_packet *pkt;
+    
+	while ((pkt = priv->ppool)) {
+		priv->ppool = pkt->next;
+		kfree (pkt);
+		/* FIXME - in-flight packets ? */
+	}
+}    
+
+/*
+ * Buffer/pool management.
+ */
+
+
+void smv_release_buffer(struct smv_packet *pkt)
+{
+	unsigned long flags;
+	struct smv_priv *priv = netdev_priv(pkt->dev);
+	
+	spin_lock_irqsave(&priv->lock, flags);
+	pkt->next = priv->ppool;
+	priv->ppool = pkt;
+	spin_unlock_irqrestore(&priv->lock, flags);
+	if (netif_queue_stopped(pkt->dev) && pkt->next == NULL)
+		netif_wake_queue(pkt->dev);
+}
+
+
+
+struct smv_packet *smv_dequeue_buf(struct net_device *dev)
+{
+	struct smv_priv *priv = netdev_priv(dev);
+	struct smv_packet *pkt;
+	unsigned long flags;
+
+	spin_lock_irqsave(&priv->lock, flags);
+	pkt = priv->rx_queue;
+	if (pkt != NULL)
+		priv->rx_queue = pkt->next;
+	spin_unlock_irqrestore(&priv->lock, flags);
+	return pkt;
+}
+
+/*
+ * Enable and disable receive interrupts.
+ */
+static void smv_rx_ints(struct net_device *dev, int enable)
+{
+	struct smv_priv *priv = netdev_priv(dev);
+	priv->rx_int_enabled = enable;
+}
+
+
+static u16 fifo_recv_data(struct fifo_local *drvdata, u32 *data)
+{
+  u32 len;
+  int i;
+	int j=900;
+	int found=0;
+  /* read data */
+
+  len = __raw_readl(drvdata->base_addr + 0x1c); // occupancy
+
+  if (len>=(36*numberSMVFrames)){
+		
+		for(i=0;i<9*numberSMVFrames;i++) //hardcoded because the packets must be 36 bytes (9words) length
+
+		{
+			*data=__raw_readl(drvdata->base_addr + 0x20);
+				if(found==1)
+					if(j<20)
+						j++;
+				if(*data==0x0003045b)
+					{
+						j=0;
+						found=1;
+					}
+
+					
+			if(j<18)
+				printk("SMV Data for %d hex %08x dec %d",i, *data,*data);
+			
+			data++;
+		}
+		return 36*numberSMVFrames; //hardcoded because the packets must be 36 bytes (9words) length
+
+	}
+	else
+		return 0;
+}
+
+
+static void fifo_rx_handler(struct net_device *dev)
+{
+
+  struct fifo_local *lp = netdev_priv(dev);
+	struct sk_buff *skb;
+  int align;
+  int len;
+  int occupancy;
+  
+
+  
+  occupancy = __raw_readl(lp->base_addr + 0x1c); //occupancy
+
+    skb = netdev_alloc_skb(dev, 2048 + ALIGNMENT);
+    if (!skb) {
+
+      return;
+    }
+    
+    /* A new skb should have the data halfword aligned, but this code is
+     * here just in case that isn't true. Calculate how many
+     * bytes we should reserve to get the data to start on a word
+     * boundary
+     */
+    align = BUFFER_ALIGN(skb->data);
+    if (align)
+      skb_reserve(skb, align);
+
+    skb_reserve(skb, 2);
+    len = fifo_recv_data(lp, (u32 *)skb->data);
+    
+    if (!len) {
+     // dev->stats.rx_errors++;
+      dev_kfree_skb_irq(skb);
+      return;
+    }
+
+    skb_put(skb, len);	/* Tell the skb how much data we got */
+
+    skb->protocol = eth_type_trans(skb, dev);
+    skb_checksum_none_assert(skb);
+
+
+    netif_rx(skb); //send the frame to upper layers
+
+}
+
+
+static irqreturn_t smv_FIFO_interrupt(int irq, void *dev_id)
+{
+
+  struct net_device *dev = dev_id;
+	struct fifo_local *lp = netdev_priv(dev);
+  void __iomem *base_addr = lp->base_addr;
+  __raw_writel(0xFFFFFFFF, base_addr);
+  fifo_rx_handler(dev);
+  return IRQ_HANDLED;
+}    
+/*
+ * Open and close
+ */
+
+int smv_open(struct net_device *dev)
+{
+	int retval;
+  /* request_region(), request_irq(), ....  (like fops->open) */
+	
+	smv_rx_ints(dev, 1);
+	
+	retval = request_irq(dev->irq, smv_FIFO_interrupt, 0, dev->name, dev);
+  if (retval) {
+		printk("Could not allocate interrupt %d\n",
+			dev->irq);
+    }  
+	/* 
+	 * Assign the hardware address of the board: use "\0SNULx", where
+	 * x is 0 or 1. The first byte is '\0' to avoid being a multicast
+	 * address (the first byte of multicast addrs is odd).
+	 */
+	memcpy(dev->dev_addr, "\0SNUL0", ETH_ALEN);
+	/*if (dev == smv_devs[1])
+		dev->dev_addr[ETH_ALEN-1]++;  \0SNUL1 */
+	netif_start_queue(dev);
+	return 0;
+}
+
+int smv_release(struct net_device *dev)
+{
+    /* release ports, irq and such -- like fops->close */
+
+	netif_stop_queue(dev); /* can't transmit any more */
+	return 0;
+}
+
+/*
+ * Configuration changes (passed on by ifconfig)
+ */
+int smv_config(struct net_device *dev, struct ifmap *map)
+{
+	if (dev->flags & IFF_UP) /* can't act on a running interface */
+		return -EBUSY;
+
+	/* Don't allow changing the I/O address */
+	if (map->base_addr != dev->base_addr) {
+		printk(KERN_WARNING "smv: Can't change I/O address\n");
+		return -EOPNOTSUPP;
+	}
+
+	/* Allow changing the IRQ */
+	if (map->irq != dev->irq) {
+		dev->irq = map->irq;
+        	/* request_irq() is delayed to open-time */
+	}
+
+	/* ignore other fields */
+	return 0;
+}
+
+/*
+ * Receive a packet: retrieve, encapsulate and pass over to upper levels
+ */
+void smv_rx(struct net_device *dev, struct smv_packet *pkt)
+{
+	struct sk_buff *skb;
+	struct smv_priv *priv = netdev_priv(dev);
+
+
+	
+	/*
+	 * The packet has been retrieved from the transmission
+	 * medium. Build an skb around it, so upper layers can handle it
+	 */
+	skb = dev_alloc_skb(pkt->datalen + 2);
+	if (!skb) {
+		if (printk_ratelimit())
+			printk(KERN_NOTICE "smv rx: low on mem - packet dropped\n");
+		priv->stats.rx_dropped++;
+		goto out;
+	}
+	skb_reserve(skb, 2); /* align IP on 16B boundary */  
+	memcpy(skb_put(skb, pkt->datalen), pkt->data, pkt->datalen);
+
+	/* Write metadata, and then pass to the receive level */
+	skb->dev = dev;
+	skb->protocol = eth_type_trans(skb, dev);
+	skb->ip_summed = CHECKSUM_UNNECESSARY; /* don't check it */
+	priv->stats.rx_packets++;
+	priv->stats.rx_bytes += pkt->datalen;
+	netif_rx(skb);
+  out:
+  
+  
+	return;
+}
+    
+
+/*
+ * The poll implementation.
+ */
+static int smv_poll(struct napi_struct *napi, int budget)
+{
+	int npackets = 0;
+	struct sk_buff *skb;
+	struct smv_priv *priv = container_of(napi, struct smv_priv, napi);
+	struct net_device *dev = priv->dev;
+	struct smv_packet *pkt;
+    
+	while (npackets < budget && priv->rx_queue) {
+		pkt = smv_dequeue_buf(dev);
+		skb = dev_alloc_skb(pkt->datalen + 2);
+		if (! skb) {
+			if (printk_ratelimit())
+				printk(KERN_NOTICE "smv: packet dropped\n");
+			priv->stats.rx_dropped++;
+			smv_release_buffer(pkt);
+			continue;
+		}
+		skb_reserve(skb, 2); /* align IP on 16B boundary */  
+		memcpy(skb_put(skb, pkt->datalen), pkt->data, pkt->datalen);
+		skb->dev = dev;
+		skb->protocol = eth_type_trans(skb, dev);
+		skb->ip_summed = CHECKSUM_UNNECESSARY; /* don't check it */
+		netif_receive_skb(skb);
+		
+        	/* Maintain stats */
+		npackets++;
+		priv->stats.rx_packets++;
+		priv->stats.rx_bytes += pkt->datalen;
+		smv_release_buffer(pkt);
+	}
+	/* If we processed all packets, we're done; tell the kernel and reenable ints */
+	if (! priv->rx_queue) {
+		napi_complete(napi);
+		smv_rx_ints(dev, 1);
+		return 0;
+	}
+	/* We couldn't process everything. */
+	return npackets;
+}
+	    
+
+        
+
+/*
+ * Transmit a packet (low level interface)
+ */
+static int smv_fifo_hw_tx(struct fifo_local *drvdata, unsigned char *buf, int len, struct net_device *dev)
+{
+
+  void __iomem *addr;
+  
+  int i;  
+  u32* data_to_write_p;
+  u32 reg_data;
+  addr = drvdata->base_addr;
+  
+	reg_data = __raw_readl(addr + 0xc);
+  if(reg_data < len)
+  {
+    return NETDEV_TX_BUSY;
+  }
+  
+  
+  __raw_writel(0xFFFFFFFF, addr);
+  __raw_writel(0x04000000, addr+0x04); // IER: only enable rx interrupts 
+  __raw_writel(0x00000000, addr+0x2C);
+  
+  __raw_writel(0x00000000, addr+0x2C);
+  data_to_write_p =(uint32_t*) buf;
+  for(i=0;i<len;i++)
+  {
+   __raw_writel(*(buf), addr+0x10);
+  
+   buf++;
+  }
+  reg_data = __raw_readl(addr + 0xc);
+  __raw_writel(len*4, addr+0x14);
+  
+  return NETDEV_TX_OK;
+ 
+}
+
+
+
+/*
+ * Transmit a packet (called by the kernel)
+ */
+int smv_fifo_tx(struct sk_buff *skb, struct net_device *dev)
+{
+  
+	return 0; 
+}
+
+
+
+/*
+ * Ioctl commands 
+ */
+int smv_ioctl(struct net_device *dev, struct ifreq *rq, int cmd)
+{
+	PDEBUG("ioctl\n");
+	return 0;
+}
+
+/*
+ * Return statistics to the caller
+ */
+struct net_device_stats *smv_stats(struct net_device *dev)
+{
+	struct smv_priv *priv = netdev_priv(dev);
+	return &priv->stats;
+}
+
+/*
+ * This function is called to fill up an eth header, since arp is not
+ * available on the interface
+ */
+
+
+static const struct net_device_ops smv_netdev_ops = {
+	.ndo_open            = smv_open,
+	.ndo_stop            = smv_release,
+	.ndo_start_xmit      = smv_fifo_tx,
+	.ndo_do_ioctl        = smv_ioctl,
+	.ndo_set_config      = smv_config,
+	.ndo_get_stats       = smv_stats,
+};
+
+/*
+ * The init function (sometimes called probe).
+ * It is invoked by register_netdev()
+ */
+void smv_init(struct net_device *dev)
+{
+	struct smv_priv *priv;
+#if 0
+    	/*
+	 * Make the usual checks: check_region(), probe irq, ...  -ENODEV
+	 * Make the usual checks: check_region(), probe irq, ...  -ENODEV
+	 * should be returned if no device found.  No resource should be
+	 * grabbed: this is done on open(). 
+	 */
+#endif
+
+    	/* 
+	 * Then, assign other fields in dev, using ether_setup() and some
+	 * hand assignments
+	 */
+	ether_setup(dev); /* assign some of the fields */
+	dev->watchdog_timeo = timeout;
+	dev->netdev_ops = &smv_netdev_ops;
+	/* keep the default flags, just add NOARP */
+	dev->flags           |= IFF_NOARP;
+	dev->features        |= NETIF_F_HW_CSUM;
+
+	/*
+	 * Then, initialize the priv field. This encloses the statistics
+	 * and a few private fields.
+	 */
+	priv = netdev_priv(dev);
+	if (use_napi) {
+		printk("SMV Use napi TRUE\n");
+		netif_napi_add(dev, &priv->napi, smv_poll,2);
+	}
+
+	
+	memset(priv, 0, sizeof(struct smv_priv));
+	spin_lock_init(&priv->lock);
+	smv_setup_pool(dev);
+}
+
+
+
+
+/*
+ * Finally, the module stuff
+ */
+
+static int  smv_cleanup(void)
+{
+	int i;
+    
+
+	for (i = 0; i < counter;  i++) {
+		if (smv_devs[i]) {
+			unregister_netdev(smv_devs[i]);
+			smv_teardown_pool(smv_devs[i]);
+			free_netdev(smv_devs[i]);
+		}
+	}
+	return 0;
+}
+
+
+
+int created = 0;
+
+static int smv_init_module(struct platform_device *ofdev)
+{
+
+  printk("Registering SMV fifo driver \n");
+  struct resource *res;
+	struct fifo_local *lp = NULL;
+	struct device *dev = &ofdev->dev;
+  
+  struct device * my_dev;
+  struct kobject root;
+  
+  u32 reg_data;
+  
+  int result, ret = -ENOMEM;
+
+
+	
+	if (created == 0)
+	{
+		// Create file number frames configuration
+		// Create /sys/devices/smv/numberSMVFrames/value
+		my_dev = root_device_register("smv");
+		root = my_dev->kobj;
+	
+		numberSMVFrames_kobject = kobject_create_and_add("numberSMVFrames",&root);
+		created = 1;
+		sysfs_create_file(numberSMVFrames_kobject, &numberSMVFrames_attribute.attr);
+	}
+
+	/* Allocate the devices */
+	if(counter < MAX_DEVICES)
+	{
+		//printk("Counter = %d\n", counter);
+		smv_devs[counter] = alloc_netdev(sizeof(struct smv_priv), "smv%d",NET_NAME_UNKNOWN,
+				smv_init);
+		//printk("[OK] alloc netdev\n");
+		if (smv_devs[counter] == NULL )
+			goto out;
+
+		dev_set_drvdata(dev, smv_devs[counter]);
+		SET_NETDEV_DEV(smv_devs[counter], &ofdev->dev);
+		lp = netdev_priv(smv_devs[counter]);
+		lp->ndev = smv_devs[counter];
+		//printk("[OK] dev_set_drvdata\n");
+  
+  res = platform_get_resource(ofdev, IORESOURCE_IRQ, 0);
+  //printk("[OK] platform get resource\n");
+	if (!res) {
+		dev_err(dev, "no IRQ found\n");
+	}
+ 
+	smv_devs[counter]->irq = res->start;
+  
+  res = platform_get_resource(ofdev, IORESOURCE_MEM, 0);
+	lp->base_addr = devm_ioremap_resource(&ofdev->dev, res);
+  
+  //vacancy=lp->base_addr+VACANCYOFFSET;
+
+	//printk("[OK] devm_ioremap_resource\n");
+	
+  //my_dev = root_device_register("smvfifo");
+  //root = my_dev->kobj;
+  //vacancy_kobject = kobject_create_and_add("Vacancy", &root );
+  //sysfs_create_file(vacancy_kobject, &vacancy_attribute.attr);
+
+	//printk("[OK] sysfs_create_file\n");
+
+	smv_devs[counter]->mem_start = res->start;
+	smv_devs[counter]->mem_end = res->end;
+	//printk("[OK] MEM_START MEM_END\n");
+	ret = -ENODEV;
+	}
+	else printk("SMV: ERROR, counter > MAX_DEVICES\n");
+	
+		if ((result = register_netdev(smv_devs[counter])))
+			printk("SMV: error %i registering device \"%s\"\n",
+					result, smv_devs[counter]->name);
+		else
+			{
+        reg_data = __raw_readl(lp->base_addr + 0x4);
+        __raw_writel(0x04000000, lp->base_addr+0x04); //IER
+        //__raw_writel(0x00400000, lp->base_addr+0x04); //IER
+
+        ret = 0;
+        printk("SMV fifo driver registered at %x",lp->base_addr);
+      }
+      counter++;
+   out:
+	if (ret) 
+		smv_cleanup();
+	
+	return ret;
+}
+
+
+/* Match table for OF platform binding */
+static const struct of_device_id smv_fifo_match[] = {
+
+	{ .compatible = "xlnx,SMV_fifo-19.05", },
+	{ /* end of list */ },
+};
+MODULE_DEVICE_TABLE(of, smv_fifo_match);
+
+static struct platform_driver smv_fifo_driver = {
+	.driver = {
+		.name = DRIVER_NAME,
+		.of_match_table = smv_fifo_match,
+	},
+	.probe		= smv_init_module,
+};
+
+module_platform_driver(smv_fifo_driver);
+
+MODULE_AUTHOR("David Esteban - Eneko Montero");
+MODULE_DESCRIPTION("SMV Fifo module");
+MODULE_LICENSE("GPL");
+
diff --git a/drivers/net/smv_fifo.h b/drivers/net/smv_fifo.h
new file mode 100644
index 000000000000..5b1d1208e169
--- /dev/null
+++ b/drivers/net/smv_fifo.h
@@ -0,0 +1,30 @@
+
+/*
+ * smv_fifo.h -- definitions for the network module
+ *
+ */
+
+#undef PDEBUG             /* undef it, just in case */
+#ifdef SNULL_DEBUG
+#  ifdef __KERNEL__
+     /* This one if debugging is on, and kernel space */
+#    define PDEBUG(fmt, args...) printk( KERN_DEBUG "snull: " fmt, ## args)
+#  else
+     /* This one for user space */
+#    define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
+#  endif
+#else
+#  define PDEBUG(fmt, args...) /* not debugging: nothing */
+#endif
+
+#undef PDEBUGG
+#define PDEBUGG(fmt, args...) /* nothing: it's a placeholder */
+
+
+
+/* Default timeout period */
+#define SMV_TIMEOUT 5   /* In jiffies */
+
+extern struct net_device *snull_devs[];
+
+
-- 
2.17.1

